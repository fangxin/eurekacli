package eurekacli


import (
	"github.com/ArthurHlt/go-eureka-client/eureka"
	"encoding/json"
	"net/http"
	"net/url"
	"path"
	"fmt"
	"bytes"
	"net"
	"time"
	"io/ioutil"
	"math/rand"
	"strings"
	"sync"
)

var	g_cli *eureka.Client
var g_addr string
var httpcli *http.Client

var g_ip_lock sync.RWMutex
var g_ips map[string][]string
var g_ip_wait map[string][]*sync.WaitGroup

func Init_eu(addr []string) error {
	g_addr = addr[0]
	cli := eureka.NewClient(addr)

	_,err := cli.GetApplications()
	if err != nil {
		return err
	}
	g_cli = cli

	transport := &http.Transport{
        Proxy: nil,
        DialContext: (&net.Dialer{
                Timeout:   10 * time.Second,
                KeepAlive: 30 * time.Second,
                DualStack: true,
        }).DialContext,
        MaxIdleConns:          2000,
		MaxIdleConnsPerHost:	2000,
        IdleConnTimeout:       120 * time.Second,
        TLSHandshakeTimeout:   10 * time.Second,
		ResponseHeaderTimeout: 20 * time.Second,
		ExpectContinueTimeout: 0 * time.Second,
	}
	httpcli = &http.Client{}
	httpcli.Transport = transport

	g_ips = make(map[string][]string)
	g_ip_wait = make(map[string][]*sync.WaitGroup)

	go fresh_cache_run()

	return nil
}
func fresh_cache_run() {
	for {
		time.Sleep(time.Second*10)
		fresh_cache()
	}
}
func fresh_cache() {
	var names []string

	g_ip_lock.RLock()
	for cur,_ := range g_ips {
		names = append(names, cur)
	}
	g_ip_lock.RUnlock()

	for _,sername := range names {
		ip,err := req_ip(sername)
		if err != nil {
			fmt.Println("get app", sername, err)
			continue
		}

		g_ip_lock.Lock()
		g_ips[sername] = ip
		g_ip_lock.Unlock()
	}
}
func Register(sername string, ip string, port int) error {
	if ip == "" {
		sip,err := get_my_ip(g_addr)
		if err != nil {
			return err
		}
		ip = sip
	}

	instance := eureka.NewInstanceInfo(ip, sername, ip, port, 30, false)
	instance.InstanceID = fmt.Sprintf("%s:%s:%d", ip, sername, port)

	err := g_cli.RegisterInstance(instance.App, instance)
	if err != nil {
		return err
	}

	err = g_cli.SendHeartbeat(instance.App, instance.InstanceID)
	if err != nil {
		return err
	}

	go func() {
		for {
			err := g_cli.SendHeartbeat(instance.App, instance.InstanceID)
			if err != nil {
				fmt.Println("eureka SendHeartbeat", err)
			}
			
			time.Sleep(time.Second*30)
		}
	}()

	return nil
}

func get_my_ip(src_addr string) (ret string, rete error) {
	u, err := url.Parse(src_addr)
	if err != nil {
		rete = err
		return
	}
	addr := u.Host
	conn,err := net.DialTimeout("tcp", addr, time.Second*5)
	if err != nil {
		rete = err
		return
	}
	addr = conn.LocalAddr().String()
	idx := strings.LastIndex(addr, ":")
	if idx <= 0 {
		rete = fmt.Errorf("%s parse error", addr)
		return
	}
	ret = addr[:idx]
	return
}

func req_ip(sername string) (ret []string, rete error) {
	app,err := g_cli.GetApplication(sername)
	if err !=  nil {
		rete = err
		return
	}

	for _,cur := range app.Instances {
		ret = append(ret, fmt.Sprintf("%s:%d", cur.IpAddr, cur.Port.Port))
	}
	return
}

func req_ip_run(sername string) {
	ip,err := req_ip(sername)
	if err != nil {
		fmt.Println("get app", sername, err)
	}

	g_ip_lock.Lock()
	g_ips[sername] = ip
	wts := g_ip_wait[sername]
	delete(g_ip_wait, sername)
	g_ip_lock.Unlock()

	for _,cur := range wts {
		cur.Done()
	}
}
func GetIP(sername string) (ret []string, rete error) {
	g_ip_lock.RLock()
	ips,ok := g_ips[sername]
	g_ip_lock.RUnlock()

	if ok {
		return ips,nil
	}

	need_req := false

	var wt sync.WaitGroup
	g_ip_lock.Lock()
	ips,ok = g_ips[sername]
	if !ok {
		wt.Add(1)
		old,ok := g_ip_wait[sername]
		if !ok {
			need_req = true
		}
		g_ip_wait[sername] = append(old, &wt)
	}
	g_ip_lock.Unlock()

	if ok {
		return ips,nil
	}

	if need_req {
		go req_ip_run(sername)
	}

	wt.Wait()

	g_ip_lock.RLock()
	ips,_ = g_ips[sername]
	g_ip_lock.RUnlock()
	
	return ips,nil
}
func GetaIP(sername string) (ret string, rete error) {
	ips,err := GetIP(sername)
	if err != nil {
		rete = err
		return
	}
	if len(ips) <= 0 {
		rete = fmt.Errorf("can't get %s", sername)
		return
	}
	
	i := rand.Intn(len(ips))

	return ips[i],nil
}
func Get(sername,page string) (ret []byte, rete error) {
	ip,err := GetaIP(sername)
	if err != nil {
		rete = err
		return
	}

	org_url := fmt.Sprintf("http://%s", ip)

	u, err := url.Parse(org_url)
	if err != nil {
		rete = err
		return
	}

	u.Path = path.Join(u.Path, page)
	full_url := u.String()	

	resp, err := httpcli.Get(full_url)

	if err != nil {
		rete = err
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		err := fmt.Errorf("%s", resp.Status)
		rete = err
		return
	}
	
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		rete = err
		return
	}

	ret = body
	return
}
func Post(sername,page string, data []byte) (ret []byte, rete error) {
	ip,err := GetaIP(sername)
	if err != nil {
		rete = err
		return
	}

	org_url := fmt.Sprintf("http://%s", ip)

	u, err := url.Parse(org_url)
	if err != nil {
		rete = err
		return
	}

	u.Path = path.Join(u.Path, page)
	full_url := u.String()	

	resp, err := httpcli.Post(full_url, "text/json", bytes.NewBuffer(data))

	if err != nil {
		rete = err
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		err := fmt.Errorf("%s", resp.Status)
		rete = err
		return
	}
	
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		rete = err
		return
	}

	ret = body
	return
}
func PostJson(sername,page string, req interface{}) (ret []byte, rete error) {
	data,err := json.Marshal(req)
	if err != nil {
		rete = err
		return
	}
	return Post(sername, page, data)
}